import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class FX extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox vBox = new VBox();

        Scene scene = new Scene(new Group(), 600, 400);
        scene.setRoot(vBox);
        primaryStage.setTitle("My First FX Maven Project");
        primaryStage.setScene(scene);
        setUserAgentStylesheet(STYLESHEET_MODENA);
        primaryStage.show();
    }
}
